﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StarterPackages.Properties;

namespace StarterPackages
{
    public partial class DVDSoftwares : Form
    {
        public DVDSoftwares()
        {
            InitializeComponent();
        }

        private async Task ShowNotification(string title, string message)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat", "@if (@X)==(@Y) @end /* JScript comment\r\n@echo off\r\n\r\nsetlocal\r\ndel /q /f %~n0.exe >nul 2>&1\r\nfor /f \"tokens=* delims=\" %%v in ('dir /b /s /a:-d  /o:-n \"%SystemRoot%\\Microsoft.NET\\Framework\\*jsc.exe\"') do (\r\n   set \"jsc=%%v\"\r\n)\r\n\r\nif not exist \"%~n0.exe\" (\r\n    \"%jsc%\" /nologo /out:\"%~n0.exe\" \"%~dpsfnx0\"\r\n)\r\n\r\nif exist \"%~n0.exe\" ( \r\n    \"%~n0.exe\" %* \r\n)\r\n\r\n\r\nendlocal & exit /b %errorlevel%\r\n\r\nend of jscript comment*/\r\n\r\nimport System;\r\nimport System.Windows;\r\nimport System.Windows.Forms;\r\nimport System.Drawing;\r\nimport System.Drawing.SystemIcons;\r\n\r\n\r\nvar arguments:String[] = Environment.GetCommandLineArgs();\r\n\r\n\r\nvar notificationText=\"Warning\";\r\nvar icon=System.Drawing.SystemIcons.Hand;\r\nvar tooltip=null;\r\n//var tooltip=System.Windows.Forms.ToolTipIcon.Info;\r\nvar title=\"\";\r\n//var title=null;\r\nvar timeInMS:Int32=2000;\r\n\r\n\r\n\r\n\r\n\r\nfunction printHelp( ) {\r\n   print( arguments[0] + \" [-tooltip warning|none|warning|info] [-time milliseconds] [-title title] [-text text] [-icon question|hand|exclamation|аsterisk|application|information|shield|question|warning|windlogo]\" );\r\n\r\n}\r\n\r\nfunction setTooltip(t) {\r\n\tswitch(t.toLowerCase()){\r\n\r\n\t\tcase \"error\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"none\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.None;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"info\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Info;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\t//tooltip=null;\r\n\t\t\tprint(\"Warning: invalid tooltip value: \"+ t);\r\n\t\t\tbreak;\r\n\t\t\r\n\t}\r\n\t\r\n}\r\n\r\nfunction setIcon(i) {\r\n\tswitch(i.toLowerCase()){\r\n\t\t //Could be Application,Asterisk,Error,Exclamation,Hand,Information,Question,Shield,Warning,WinLogo\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"application\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Application;\r\n\t\t\tbreak;\r\n\t\tcase \"аsterisk\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Asterisk;\r\n\t\t\tbreak;\r\n\t\tcase \"error\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"exclamation\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Exclamation;\r\n\t\t\tbreak;\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"information\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Information;\r\n\t\t\tbreak;\r\n\t\tcase \"question\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Question;\r\n\t\t\tbreak;\r\n\t\tcase \"shield\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Shield;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"winlogo\":\r\n\t\t\ticon=System.Drawing.SystemIcons.WinLogo;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\tprint(\"Warning: invalid icon value: \"+ i);\r\n\t\t\tbreak;\t\t\r\n\t}\r\n}\r\n\r\n\r\nfunction parseArgs(){\r\n\tif ( arguments.length == 1 || arguments[1].toLowerCase() == \"-help\" || arguments[1].toLowerCase() == \"-help\"   ) {\r\n\t\tprintHelp();\r\n\t\tEnvironment.Exit(0);\r\n\t}\r\n\t\r\n\tif (arguments.length%2 == 0) {\r\n\t\tprint(\"Wrong number of arguments\");\r\n\t\tEnvironment.Exit(1);\r\n\t} \r\n\tfor (var i=1;i<arguments.length-1;i=i+2){\r\n\t\ttry{\r\n\t\t\t//print(arguments[i] +\"::::\" +arguments[i+1]);\r\n\t\t\tswitch(arguments[i].toLowerCase()){\r\n\t\t\t\tcase '-text':\r\n\t\t\t\t\tnotificationText=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-title':\r\n\t\t\t\t\ttitle=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-time':\r\n\t\t\t\t\ttimeInMS=parseInt(arguments[i+1]);\r\n\t\t\t\t\tif(isNaN(timeInMS))  timeInMS=2000;\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-tooltip':\r\n\t\t\t\t\tsetTooltip(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-icon':\r\n\t\t\t\t\tsetIcon(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tdefault:\r\n\t\t\t\t\tConsole.WriteLine(\"Invalid Argument \"+arguments[i]);\r\n\t\t\t\t\tbreak;\r\n\t\t}\r\n\t\t}catch(e){\r\n\t\t\terrorChecker(e);\r\n\t\t}\r\n\t}\r\n}\r\n\r\nfunction errorChecker( e:Error ) {\r\n\tprint ( \"Error Message: \" + e.message );\r\n\tprint ( \"Error Code: \" + ( e.number & 0xFFFF ) );\r\n\tprint ( \"Error Name: \" + e.name );\r\n\tEnvironment.Exit( 666 );\r\n}\r\n\r\nparseArgs();\r\n\r\nvar notification;\r\n\r\nnotification = new System.Windows.Forms.NotifyIcon();\r\n\r\n\r\n\r\n//try {\r\n\tnotification.Icon = icon; \r\n\tnotification.BalloonTipText = notificationText;\r\n\tnotification.Visible = true;\r\n//} catch (err){}\r\n\r\n \r\nnotification.BalloonTipTitle=title;\r\n\r\n\t\r\nif(tooltip!==null) { \r\n\tnotification.BalloonTipIcon=tooltip;\r\n}\r\n\r\n\r\nif(tooltip!==null) {\r\n\tnotification.ShowBalloonTip(timeInMS,title,notificationText,tooltip); \r\n} else {\r\n\tnotification.ShowBalloonTip(timeInMS);\r\n}\r\n\t\r\nvar dieTime:Int32=(timeInMS+100);\r\n\t\r\nSystem.Threading.Thread.Sleep(dieTime);\r\nnotification.Dispose();");
            RunCommand("call \"" + Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat" +
                                   "\"   -tooltip warning -time 3000 -title \"" + title + "\" -text \"" + message +
                                   "\" -icon question");
        }

        public async Task RunCommand(string Command)
        {
            bool e = false;
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat", CommandChut);
            Process dew = new Process();
            dew.StartInfo.FileName =
                System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat";
            dew.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            dew.Exited += (sender, args) =>
            {
                e = true;
            };
            dew.EnableRaisingEvents = true;
            dew.Start();
            while (e == false)
            {
                await Task.Delay(10);
            }

            e = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat");
        }

        private async Task DeleteFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        private async Task DownloadMulti(string Database, string Filename, string Parts)
        {
            string[] Stuff = { Database, Filename, Parts, "false", "Blank" };
            File.WriteAllLines(User + "\\AppData\\Local\\Temp\\SilentInstallD.txt", Stuff);
            await Encode(User + "\\AppData\\Local\\Temp\\SilentInstallD.txt", User + "\\AppData\\Local\\Temp\\SilentInstall.txt");
            await StartMultiDownloader();
        }

        private async Task PlaySoundFromDataBaseSync(string SoundName)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri("https://raw.githubusercontent.com/EpicGamesGun/Starter-Packages-Sound-Database/master/" + SoundName + ".wav"),
                        Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                    while (client.IsBusy)
                    {
                        await Task.Delay(10);
                    }
                }

                SoundPlayer dew = new SoundPlayer(Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                await Task.Factory.StartNew(() => { dew.PlaySync(); });
            }
            catch
            {

            }
        }

        private async Task PlaySoundFromDataBase(string SoundName)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri("https://raw.githubusercontent.com/EpicGamesGun/Starter-Packages-Sound-Database/master/" + SoundName + ".wav"),
                        Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                    while (client.IsBusy)
                    {
                        await Task.Delay(10);
                    }
                }

                SoundPlayer dew = new SoundPlayer(Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                dew.Play();
            }
            catch
            {

            }
        }

        private async Task<bool> PlaceTextDocument()
        {
            bool Return = false;
            await Task.Delay(10000);
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
            {
                Return = true;
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
            }
            else
            {
                Return = false;
            }

            return Return;
        }

        private async Task Encode(string Input, string Output)
        {
            await RunCommandHidden("certutil -encode \"" + Input + "\" \"" + Output + "\"");
        }
        private async Task Decode(string Input, string Output)
        {
            await RunCommandHidden("certutil -decode \"" + Input + "\" \"" + Output + "\"");
        }

        private bool Exit = false;
        public async Task RunCommandHidden(string Command)
        {
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat", CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }

        private string DefaultVeracryptEXEPath = "\"C:\\Program Files\\VeraCrypt\\VeraCrypt.exe\"";
        private async Task MountVeracrypt(string Path, string Password, string PIM, string Letter)
        {
            await RunCommandHidden(DefaultVeracryptEXEPath + " /q /v \"" + Path + "\" /l " + Letter + " /a /p " + Password + " /pim " + PIM + "");
        }
        private async Task DismountVeracrypt(string Letter)
        {
            await RunCommandHidden("\"C:\\Program Files\\VeraCrypt\\VeraCrypt.exe\" /q /d " + Letter + " /force /s");
        }

        private async Task CreateVeracryptVolume(string Path, string Password, string Size, string PIM)
        {
            await RunCommandHidden("\"C:\\Program Files\\VeraCrypt\\VeraCrypt Format.exe\" /create " + Path +
                                   " /password " + Password + " /pim " + PIM + " /hash sha512 /encryption serpent /filesystem FAT /size " + Size + "M /force /silent /quick");
        }
        private async Task Download(string link, string filename)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri(link), filename);
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
        }

        private async Task InstallChocolatey()
        {
            await CheckInternet();
            File.WriteAllText("C:\\InstallingChocolate", "true");
            if (File.Exists("C:\\HoldOn.txt"))
            {
                while (File.Exists("C:\\HoldOn.txt"))
                {
                    await Task.Delay(10);
                }
            }
            if (!Directory.Exists("C:\\ProgramData\\chocolatey"))
            {
                await RunCommandHidden(
                    "@\"%SystemRoot%\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command \" [System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))\" && SET \"PATH=%PATH%;%ALLUSERSPROFILE%\\chocolatey\\bin\"");
                await PlaySoundFromDataBaseSync("Software Successfully Installed");
            }
            File.Delete("C:\\InstallingChocolate");
        }

        private async Task ChocolateyInstall(string Software)
        {
            string AlreadyInstalledDirectory =
                Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\InstalledSoftware";
            try
            {
                if (!Directory.Exists(AlreadyInstalledDirectory))
                {
                    Directory.CreateDirectory(AlreadyInstalledDirectory);
                }

                if (!File.Exists(AlreadyInstalledDirectory + "\\" + Software))
                {
                    await CheckInternet();
                    bool Failed = false;
                    bool Warning = false;
                    bool AlreadyInstalled = false;
                    await RunCommandHidden("cd \"C:\\ProgramData\\chocolatey\" \n>" + "C:\\InstallationLog.txt (\n" +
                                           "choco.exe install " + Software + " -y --ignore-checksums\n)");
                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                    {
                        if (readLine.Contains("already installed"))
                        {
                            AlreadyInstalled = true;
                        }

                    }

                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                    {
                        if (readLine.Contains("Chocolatey installed 0"))
                        {
                            if (AlreadyInstalled == false)
                            {
                                Failed = true;
                            }
                        }
                    }

                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                    {
                        if (readLine.Contains("Warnings"))
                        {
                            if (Failed == true != AlreadyInstalled == true)
                            {

                            }
                            else
                            {
                                Warning = true;
                            }
                        }
                    }

                    if (Failed == true)
                    {
                        Failed = false;
                        ShowNotification("FATAL ERROR", Software + " could not be installed");
                        await PlaySoundFromDataBaseSync("SoftwareFailedToInstall");
                        while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                            "\\New Text Document.txt"))
                        {
                            await Task.Delay(10);
                        }
                        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                    "\\New Text Document.txt");
                    }

                    if (Warning == true)
                    {
                        Warning = false;
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        ShowNotification("Software Installed With Warnings",
                            Software + " was successfully installed with 1 or more warnings.");
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        await PlaySoundFromDataBaseSync("InstalledWithWarnings");
                    }
                    else if (AlreadyInstalled == true)
                    {
                        AlreadyInstalled = false;
                        ShowNotification("Software Successfully Installed", Software + " was successfully installed");

                        Failed = false;

                        await PlaySoundFromDataBaseSync("SoftwareAlreadyInstalled");

                        File.WriteAllText(AlreadyInstalledDirectory + "\\" + Software, "true");
                    }
                    else
                    {
                        ShowNotification("Software Successfully Installed", Software + " was successfully installed");

                        if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Chinese.txt"))
                        {
                            SoundPlayer chinese = new SoundPlayer(Resources.ChineseSoftwareSucess);
                            chinese.PlaySync();
                        }
                        else
                        {
                            await PlaySoundFromDataBaseSync("Software Successfully Installed");
                        }

                        File.WriteAllText(AlreadyInstalledDirectory + "\\" + Software, "true");
                    }
                }
            }
            catch
            {
                ShowNotification("FATAL ERROR", Software + " could not be installed");
                await PlaySoundFromEncryptedDirectory("FailedToInstall");
                while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
                {
                    await Task.Delay(10);
                }
            }
        }

        private async Task PlaySound(SoundPlayer dew)
        {
            dew.PlaySync();
        }

        private async Task PlaySoundStreamSync(Stream location)
        {
            SoundPlayer dew = new SoundPlayer(location);
            dew.PlaySync();
        }

        private async Task CheckInternet()
        {
            bool Internet = false;
            string StringDownload = String.Empty;
            while (Internet == false)
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            StringDownload = client.DownloadString(new Uri(
                                "https://raw.githubusercontent.com/EpicGamesGun/StarterPackages/master/InternetCheck.txt"));
                        }
                        catch
                        {

                        }
                    }
                });


                if (StringDownload == "true")
                {
                    Internet = true;
                }

                await Task.Delay(1000);
            }

            Internet = false;
        }

        private string UniqueHashing(string inputstring)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://raw.githubusercontent.com/EpicGamesGun/Unique-Hasher/master/Unique-Hasher/bin/Debug/Unique-Hasher.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe");
                while (client.IsBusy)
                {
                    Task.Delay(10);
                }
            }
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Hashing.txt", inputstring);

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe").WaitForExit();

            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt").ElementAtOrDefault(0);
            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt");
        }

        public static void CreateShortcut(string shortcutName, string shortcutPath, string targetFileLocation, string Description)
        {
            string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcut.Description = Description;   // The description of the shortcut
            shortcut.TargetPath = targetFileLocation;                 // The path of the file that will launch when the shortcut is run
            shortcut.Save();                                    // Save the shortcut
        }
        private async Task PlaySoundFromEncryptedDirectory(string soundname)
        {
            if (!Directory.Exists(@"C:\Program Files\VeraCrypt"))
            {
                await ChocolateyInstall("veracrypt");
                await PlaySoundStreamSync(Resources.Veracrypt);
            }
            if (!Directory.Exists("B:\\"))
            {
                await Download("https://raw.githubusercontent.com/EpicGamesGun/StarterPackagesDatabase1/master/Sounds",
                    Environment.GetEnvironmentVariable("TEMP") + "\\Sounds");
                await MountVeracrypt(Environment.GetEnvironmentVariable("TEMP") + "\\Sounds", UniqueHashing("sounds"),
                    "500", "B");
            }

            if (File.Exists("B:\\" + soundname + ".wav"))
            {
                SoundPlayer play1 = new SoundPlayer("B:\\" + soundname + ".wav");
                await Task.Factory.StartNew(() =>
                {
                    play1.PlaySync();
                });
            }
            else if (File.Exists("B:\\" + soundname))
            {
                SoundPlayer play2 = new SoundPlayer("B:\\" + soundname);
                await Task.Factory.StartNew(() =>
                {
                    play2.PlaySync();
                });
            }
        }

        private async Task CloseEncryptedSoundDirectory()
        {
            await DismountVeracrypt("B");
        }
        private string User = Environment.GetEnvironmentVariable("USERPROFILE");
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        private async Task StartMultiDownloader()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://raw.githubusercontent.com/EpicGamesGun/JacksonDownloadManager/master/JacksonDownloadManager/bin/Debug/JacksonDownloadManager.exe"), User + "\\AppData\\Local\\Temp\\Download.exe");
                client.DownloadFileCompleted += Client_DownloadFileCompleted1;
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            await Task.Factory.StartNew(() =>
            {
                Process.Start(User + "\\AppData\\Local\\Temp\\Download.exe").WaitForExit();
            });
        }

        private async void Client_DownloadFileCompleted1(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {

            }
            else
            {

            }
        }
        private async void DVDSoftwares_Load(object sender, EventArgs e)
        {
            ShowInTaskbar = false;
            Visible = false;
            await PlaySoundFromDataBaseSync("Alcohol120Permission");
            if (await PlaceTextDocument() == true)
            {
                File.WriteAllText(Environment.GetEnvironmentVariable("APPDATA") + "\\Alcohol.txt","true");
                await PlaySoundFromDataBaseSync("AlcoholRestart");
                await Download(
                    "https://raw.githubusercontent.com/EpicGamesGun/StarterPackages/master/Resources/YWxjb2hvbA0K",
                    Environment.GetEnvironmentVariable("TEMP") + "\\Alcohol");
                await MountVeracrypt(Environment.GetEnvironmentVariable("TEMP") + "\\Alcohol", UniqueHashing("alcohol"),
                    "500", "A");
                while (!Directory.Exists("A:\\"))
                {
                    await Task.Delay(10);
                }
                Process.Start("A:\\Alcohol.exe", "/S").WaitForExit();
            }
            Close();
        }
    }
}
