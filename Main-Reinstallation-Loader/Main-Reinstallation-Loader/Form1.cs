﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main_Reinstallation_Loader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private async Task CheckInternet()
        {
            bool Internet = false;
            string StringDownload = String.Empty;
            while (Internet == false)
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            StringDownload = client.DownloadString(new Uri(
                                "http://www.msftncsi.com/ncsi.txt"));
                        }
                        catch
                        {

                        }
                    }
                });


                if (StringDownload == "Microsoft NCSI")
                {
                    Internet = true;
                }

                await Task.Delay(1000);
            }

            Internet = false;
        }

        private async Task ForceAdmin(string path)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("USERPROFILE") + "\\AppData\\Local\\Temp\\FileToRun.txt", path);
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/GetAdminRights/GetAdminRights/bin/Debug/GetAdminRights.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\AdminRights.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\AdminRights.exe").WaitForExit();
            });

        }
        private async void Form1_Load(object sender, EventArgs e)
        {
            //if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) +
            //                 "\\Main-Reinstallation-Loader.exe"))
            //{
            //    File.Copy(Application.ExecutablePath, Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\Main-Reinstallation-Loader.exe");
            //}
            if (File.Exists(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Install.txt"))
            {
                WindowsCreationDateLabel.Text += " " + File.ReadAllText("C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\\Install.txt");
            }
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                            "\\InstalledWindows.txt"))
            {
                Application.Exit();
            }
            progressBar1.Style = ProgressBarStyle.Marquee;
            await CheckInternet();
            progressBar1.Style = ProgressBarStyle.Blocks;
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += (o, args) =>
                {
                    progressBar1.Value = args.ProgressPercentage;
                };
                client.DownloadFileAsync(new Uri("http://starterpackages.bigheados.com/everything.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\Everything.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            await CheckInternet();
            await ForceAdmin(Environment.GetEnvironmentVariable("TEMP") + "\\Everything.exe");
            Application.Exit();
        }
    }
}
