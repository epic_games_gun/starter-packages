﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EverythingSetupper.Properties;

namespace EverythingSetupper
{
    public partial class SelectComponents : Form
    {
        public SelectComponents()
        {
            InitializeComponent();
        }

        public string SoftwareStoreCheck { get; set; }

        private string User = System.Environment.GetEnvironmentVariable("USERPROFILE");

        private void WriteTrue(string Name)
        {
            File.WriteAllText(User + "\\AppData\\Local\\Temp\\" + Name + ".txt", "true");
        }

        private void WriteFalse(string Name)
        {
            if (File.Exists(User + "\\AppData\\Local\\Temp\\" + Name + ".txt"))
            {
                File.Delete(User + "\\AppData\\Local\\Temp\\" + Name + ".txt");
            }
        }
        private async void SelectComponents_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            TopMost = true;
            SoftwareStoreOff.Visible = false;
            InternetCheckerOff.Visible = false;
            Office365Off.Visible = false;
            FileVaultOff.Visible = false;
            await Task.Factory.StartNew(() =>
            {
                SoundPlayer dew = new SoundPlayer(Resources.Select_Software);
                dew.PlaySync();
            });
        }

        private bool DaddyAlarm = true;
        public bool SoftwareStore = true;
        private bool InternetChecker = true;
        private bool Office = true;
        private void SoftwareStoreOn_Click(object sender, EventArgs e)
        {
            SoftwareStoreOn.Visible = false;
            SoftwareStoreOff.Visible = true;
            SoftwareStore = false;
        }


        private void SoftwareStoreOff_Click(object sender, EventArgs e)
        {
            SoftwareStore = true;
            SoftwareStoreOn.Visible = true;
            SoftwareStoreOff.Visible = false;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;

            SoundPlayer makesure = new SoundPlayer(Resources.PleaseMakeSure);
            await Task.Factory.StartNew(() =>
            {
                makesure.PlaySync();
            });
            SoundPlayer SelectedSoftwareS = new SoundPlayer(Resources.InstallingSelectedSoftware);
            UserSettings s = new UserSettings();
            s.Show();
            while (!File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\ClosedUserSettings.txt"))
            {
                await Task.Delay(10);
            }

            File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\ClosedUserSettings.txt");
            TopMost = false;
            await Task.Factory.StartNew(() =>
            {
                SelectedSoftwareS.PlaySync();
            });
            if (SoftwareStore == true)
            {
                WriteTrue("SoftwareStore");
                File.WriteAllText(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\SoftwareStore.txt","true");
                Console.WriteLine("Software Store True");
            }

            if (InternetChecker == true)
            {
                WriteTrue("InternetChecker");
                Console.WriteLine("Internet Checker True");
            }

            if (Office == true)
            {
                WriteTrue("Office");
                Console.WriteLine("Office True");
            }

            if (BitDefender == true)
            {
                WriteTrue("Bitdefender");
                Console.WriteLine("BitDefender True");
            }

            if (ActivateWindows == true)
            {
                WriteTrue("ActivateWindows");
                Console.WriteLine("Activate Windows True");
            }

            if (ComputerMonitor == true)
            {
                WriteTrue("ComputerMonitor");
                Console.WriteLine("Computer Monitor True");
            }

            if (FileVault == true)
            {
                WriteTrue("FileVault");
                Console.WriteLine("File Vault True");
            }
            Close();
        }

        private void InternetCheckerOff_Click(object sender, EventArgs e)
        {
            InternetCheckerOff.Visible = false;
            InternetCheckerOn.Visible = true;
            InternetChecker = true;
        }

        private void InternetCheckerOn_Click(object sender, EventArgs e)
        {
            InternetCheckerOff.Visible = true;
            InternetCheckerOn.Visible = false;
            InternetChecker = false;
        }

        private void Office365On_Click(object sender, EventArgs e)
        {
            Office365On.Visible = false;
            Office365Off.Visible = true;
            Office = false;
        }

        private void Office365Off_Click(object sender, EventArgs e)
        {
            Office365On.Visible = true;
            Office365Off.Visible = false;
            Office = true;
        }






        private bool BitDefender = true;
        private void BitdefenderON_Click(object sender, EventArgs e)
        {
            BitdefenderON.Visible = false;
            BitdefenderOff.Visible = true;
            BitDefender = false;
        }

        private void BitdefenderOff_Click(object sender, EventArgs e)
        {
            BitdefenderON.Visible = true;
            BitdefenderOff.Visible = false;
            BitDefender = true;
        }

        private bool ActivateWindows = true;
        private void ActivateWindowsOn_Click(object sender, EventArgs e)
        {
            ActivateWindowsOn.Visible = false;
            ActivateWindowsOff.Visible = true;
            ActivateWindows = false;
        }

        private void ActivateWindowsOff_Click(object sender, EventArgs e)
        {
            ActivateWindowsOn.Visible = true;
            ActivateWindowsOff.Visible = false;
            ActivateWindows = true;
        }

        private bool ComputerMonitor = true;
        private void ComputerMonitorOn_Click(object sender, EventArgs e)
        {
            ComputerMonitorOn.Visible = false;
            ComputerMonitorOff.Visible = true;
            //ComputerMonitor = false;
        }

        private void ComputerMonitorOff_Click(object sender, EventArgs e)
        {
            ComputerMonitorOn.Visible = true;
            ComputerMonitorOff.Visible = false;
            ComputerMonitor = true;
        }

        private bool FileVault = true;
        private void FileVaultOn_Click(object sender, EventArgs e)
        {
            //FileVaultOn.Visible = false;
            //FileVaultOff.Visible = true;
            //FileVault = false;
        }

        private void FileVaultOff_Click(object sender, EventArgs e)
        {
            FileVaultOn.Visible = true;
            FileVaultOff.Visible = false;
            FileVault = true;
        }
    }
}
