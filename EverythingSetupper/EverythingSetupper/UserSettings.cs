﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EverythingSetupper.Properties;

namespace EverythingSetupper
{
    public partial class UserSettings : Form
    {
        public UserSettings()
        {
            InitializeComponent();
            Closing += UserSettings_Closing;
        }

        private bool NeedToClose = false;
        private void UserSettings_Closing(object sender, CancelEventArgs e)
        {
            if (NeedToClose == false)
            {
                e.Cancel = true;
            }
            else
            {
                Close();
            }
        }

        private async void PlaySound(Stream location)
        {
            SoundPlayer dew = new SoundPlayer(location);
            dew.Play();
        }

        private async Task PlaySoundSync(Stream location)
        {
            SoundPlayer dew = new SoundPlayer(location);
            await Task.Factory.StartNew(() => { dew.PlaySync(); });
        }

        private async void UserSettings_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            CurrentComputerAndUsername.Text = "Current Username: \"" + Environment.UserName +
                                              "\"\nCurrent Computer Name: \"" + Environment.MachineName + "\"";
            TopMost = true;
            await PlaySoundSync(Resources.ChooseSettings);
        }


        private async void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            TopMost = false;
            if (CreateWindowsPassword.Text == ConfirmWindowsPassword.Text)
            {
                if (StarterPackagesCheckBox.Checked == false)
                {
                    File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\NoStarter","jer");
                }
                if (CreateWindowsPassword.Text == "")
                {
                    await RunCommandHidden("net user " + Environment.UserName + " \"\"");
                }
                else
                {
                    await RunCommandHidden("net user " + Environment.UserName + " " + CreateWindowsPassword.Text);
                }

                if (NewWindowsUsername.Text == "")
                {
                    
                }
                else
                {
                    await RunCommandHidden("wmic useraccount where name='" + Environment.UserName + "' rename " + NewWindowsUsername.Text);
                }
                if (AdminAccount.Checked == true)
                {

                }

                string WindowsEditionSelected()
                {
                    string Return = "";
                    if (WindowsEdition.Text == "Home")
                    {
                        Return = "TX9XD-98N7V-6WMQ6-BX7FG-H8Q99";
                    }
                    else if (WindowsEdition.Text == "Pro")
                    {
                        Return = "W269N-WFGWX-YVC9B-4J6C9-T83GX";
                    }
                    else if (WindowsEdition.Text == "Education")
                    {
                        Return = "NW6C2-QMPVW-D7KKK-3GKT6-VCFB2";
                    }
                    else if (WindowsEdition.Text == "Enterprise")
                    {
                        Return = "NPPR9-FWDCX-D2C8J-H872K-2YT43";
                    }
                    else
                    {
                        Return = "W269N-WFGWX-YVC9B-4J6C9-T83GX";
                    }

                    return Return;
                }

                if (ActivateWindows.Checked == true)
                {
                    await RunCommandHiddenNoWait("slmgr /ipk " + WindowsEditionSelected() +
                                           "\nslmgr /skms kms8.msguides.com\nslmgr /ato");
                }

                if (CreateUserAccounts == true)
                {
                    string[] UserAccounts = UserAccountListBox.Items.OfType<string>().ToArray();
                    File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\UserAccounts.txt",UserAccounts);
                    string Username = "";
                    string Password = "";
                    foreach (var readLine in File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\UserAccounts.txt"))
                    {
                        Username = readLine.Split(':')[0].Trim().ToString();
                        Password = await EncodeDecodeToBase64String(readLine.Split(':')[1].Trim().ToString(),false);
                        await RunCommandHidden("net user " + Username + " " + Password + " /add");
                    }
                }

                if (AdminAccount.Checked == true)
                {
                    await RunCommandHidden("net user administrator /active:yes");
                    await RunCommandHidden("net user administrator " + CreateWindowsPassword.Text);
                }

                if (GuestAccount.Checked == true)
                {
                    await RunCommandHidden("net user guest /active:yes");
                }
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\ClosedUserSettings.txt","");
                NeedToClose = true;
                Visible = false;
            }
        }

        private async Task<string> EncodeDecodeToBase64String(string input, bool Encode)
        {
            string Return = "";
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt", input);
            if (Encode == true)
            {
                await RunCommandHidden("certutil -encode \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encode.txt" + "\" \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encoded.txt\"");
                Return = File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt")
                    .ElementAtOrDefault(1);
                Return = Return.Replace("=", "");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            }
            else
            {
                await RunCommandHidden("certutil -decode \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encode.txt" + "\" \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encoded.txt\"");
                Return = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            }

            return Return;
        }
        // Usage Example: string Jerjer = await EncodeDecodeToBase64String(input, [true or false])//
        //true = encode, false = decode//

        private bool Exit = false;
        public async Task RunCommandHidden(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(0000, 9999);
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat", CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat");
        }

        private bool Exitt = false;
        public async Task RunCommandHiddenNoWait(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(0000, 9999);
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat", CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            //while (Exit == false)
            //{
            //    await Task.Delay(10);
            //}

            Exitt = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }

        private async void AdminAccount_CheckedChanged(object sender, EventArgs e)
        {
            if (CreateWindowsPassword.Text == "")
            {
                AdminAccount.Checked = false;
                AdminAccount.Text = "You must create a user password";
                await Task.Delay(3000);
                AdminAccount.Text = "Enable Built-In Administrator Account";
            }
        }
        bool CreateUserAccounts = false;
        private async void button2_Click(object sender, EventArgs e)
        {
            
            if (NewPassword.Text == NewConfirmPassword.Text)
            {
                CreateUserAccounts = true;
                UserAccountListBox.Items.Add(NewUsername.Text + ":" + await EncodeDecodeToBase64String(NewPassword.Text,true));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var VARIABLE in UserAccountListBox.SelectedItems.OfType<string>().ToList())
                {
                    UserAccountListBox.Items.Remove(VARIABLE);
                }
            }
            catch
            {

            }
        }
    }
}
